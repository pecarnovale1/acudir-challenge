# Acudir Challenge

## Composicion
- El proyecto se encuentra dividido en 2 partes (Frontend y Backend)
- El proyecto Acudir.API corre en el puerto 44366, desde swagger pueden verse todos los endpoints disponibles
- El backend esta generado mediante la utilizacion Repositorio Generico y Unidad de Trabajo en EF Core
- El frontend esta desarrollado bajo React JS con BootstrapCDN, utilizando AxiosClient, SweetAlert, Context y Reducers

## Implementacion local

- Agregar la base de datos en SQLServer (Acudir.bak)
- Abrir el proyecto del backend con una version posterior a VS2019 y correrlo directamente
- Modificar el appsetings.json para apuntarlo a la base de datos correspondiente
- Abrir el proyecto de front en VSCode, ejecutar npm install y npm start
- En el archivo .env.developments se encuentra la url donde apunta el Front al back