import React, { useContext  } from 'react';
import PersonasContext from '../context/vehiculos/PersonaContext';
import SpinnerSm from './SpinnerSm';

const PersonasVistas = () => {

    const {
        vistos,
        persona,
        loadingPersona
    } = useContext(PersonasContext);


    return (
        <div className="col-md-6  col-sm-12">
            {loadingPersona ||  persona == null ? 
                <SpinnerSm /> 
                :  
                <>
                
                {vistos && vistos.length > 0 && vistos.map(visto =>(
                    <div className="card m-2">
                        <div className="card-block">
                            <div className="row">
                                <div className="col-md-4 col-sm-4 text-center">
                                    <img className="btn-md" src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png" alt="" style={{padding:10,width:100}} />
                                </div>  
                                <div className="col-md-8 col-sm-8">
                                    <h2 className="card-title">Nombre: {visto.nombre} {visto.apellido}</h2>
                                    <p className="card-text"><strong>Provincia: </strong> {visto.provincia} </p>
                                </div>
                            </div>  
                        </div>
                    </div>
                )) }
                { vistos &&  vistos.length == 0 && <strong>No hay consultas anteriores</strong>}
                </>
            }
        </div>    
        );
};

export default PersonasVistas;