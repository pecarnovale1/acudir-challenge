import React from 'react';
const SpinnerSm = () => {
    return (
        <div className="d-block">
            <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    );
};

export default SpinnerSm;