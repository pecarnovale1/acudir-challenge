import React, { useContext, useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import PersonasContext from '../context/vehiculos/PersonaContext';
import Spinner from './Spinner';

const PersonaActual = () => {

    const {
        persona,
        loadingPersona,
        getNextPersona
    } = useContext(PersonasContext);


    useEffect(() => {
        getNextPersona();
    }, []);

    return (
        <div className="col-md-6 col-sm-12">
            {loadingPersona || persona == null ? 
                <Spinner /> 
            :  
                <>
                    <div className="card m-2">
                        <div className="card-block">
                            <div className="row">
                                <div className="col-md-4 col-sm-4 text-center">
                                    <img className="btn-md" src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png" alt="" style={{marginTop:15,width:100}} />
                                </div>  
                                <div className="col-md-8 col-sm-8">
                                    <h2 className="card-title">Nombre: {persona.nombre} {persona.apellido}</h2>
                                    <p className="card-text"><strong>Provincia: </strong> {persona.provincia} </p>
                                    <p className="card-text"><strong>DNI: </strong> {persona.dni}</p>
                                    <p><strong>Plataforma: </strong>
                                    <span className="badge bg-primary p-2 m-1 text-white">WordPress</span> 
                                    <span className="badge bg-info p-2 m-1  text-white">Weebly</span>
                                    <span className="badge bg-warning p-2 m-1  text-white">Bootstrap</span>
                                    <span className="badge bg-success p-2 m-1  text-white">Wix</span>
                                    </p>
                                </div>
                            </div>  
                            <div className="row">   
                                <div className="col-12 text-center">
                                    <button type="button" className="btn btn-danger btn-block btn-md" onClick={ () => getNextPersona()}><span className="fa fa-next"></span> Siguiente </button>  
                                </div>
                            </div>
                        </div>
                    </div>
                
                </>
            }
        </div>
        );
        
};

export default PersonaActual;