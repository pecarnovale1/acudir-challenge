import React from 'react';
import PersonaActual from './components/PersonaActual';
import PersonasVistas from './components/PersonasVistas';
import PersonaState from './context/vehiculos/PersonaState';
import Layout from './layout/Layout';

const App = () => {
    return (
      <PersonaState>
        <Layout>
          <PersonaActual />
          <PersonasVistas />
        </Layout>
      </PersonaState>
    );
};

export default App;