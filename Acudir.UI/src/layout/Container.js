import React from 'react';

const Container = ({children}) => {
    return (
        <main role="main">
            <div className="container-fluid p-5">
                  <div className="row">
                    {children}
                  </div>
            </div>
        </main>
    );
};

export default Container;