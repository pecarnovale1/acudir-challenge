import React from 'react';
import Header from './Header';
import Container from './Container';
import Footer from './Footer';

const Layout = ({children}) => {
    return (
       <>
        <Header />
        <Container>{children}</Container>
        <Footer />
       </>
    );
};

export default Layout;