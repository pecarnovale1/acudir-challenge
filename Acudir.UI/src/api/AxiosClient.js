import axios from 'axios';
import Swal from 'sweetalert2';

const  AxiosClient =  axios.create({
    baseURL: process.env.REACT_APP_BACKEND_URL
});


AxiosClient.interceptors.response.use(function (response) {
    return response;
}, async (error) => {
    if(error.response) {

                if (error.response.status >= 400 && error.response.status < 500) {
                    return Promise.reject(error);
                } else {
                    let errorObject = error.response.data;
                    Swal.fire('Error',errorObject.message,'error');
                    return Promise.reject(error);
                }
    } else {
        Swal.fire('Error de red','Ocurrio un error al acceder a la infomacion','error');
    }
    
});

export default AxiosClient;