import AxiosClient from '../AxiosClient';

const GetActiva = (id) => {
    return AxiosClient.get(`Persona`)
    .then(response => response.data);
}

const Delete = (id) => {
    return AxiosClient.delete(`Persona?id=${id}`)
        .then(response => response.data);
}


export  {
    GetActiva,
    Delete
}