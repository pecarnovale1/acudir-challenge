import {GET_PERSONA_REQUEST, GET_PERSONA_SUCCESS, SET_VISTO_PERSONA_ACTUAL } from "./PersonaTypes";

const PersonaReducer = (state,action) => {
    switch(action.type) {

        case GET_PERSONA_REQUEST:
            return {
                ...state,
                loadingPersona:true
            };

        case GET_PERSONA_SUCCESS:
            return {
                ...state,
                persona:action.payload,
                loadingPersona:false
            };

        case SET_VISTO_PERSONA_ACTUAL:
            return {
                ...state,
                vistos:[state.persona,...state.vistos]
            };

        default:
            return state;
    }
}

export default PersonaReducer;