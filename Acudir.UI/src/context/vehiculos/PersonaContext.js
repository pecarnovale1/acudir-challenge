import { createContext } from "react";

const PersonasContext = createContext();

export default PersonasContext;