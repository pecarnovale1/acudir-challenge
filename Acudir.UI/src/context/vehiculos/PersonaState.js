import React, { useReducer } from 'react';
import Swal from 'sweetalert2';
import { GetActiva } from '../../api/services/PersonaService';
import PersonasContext from './PersonaContext';
import PersonaReducer from './PersonaReducer';
import { GET_PERSONA_REQUEST, GET_PERSONA_SUCCESS, SET_VISTO_PERSONA_ACTUAL } from './PersonaTypes';

const PersonaState = (props) => {

    const initialState = {
        persona:null,
        vistos:[],
        loadingPersona:false,
        loadingVistos:false
    }

    const [state,dispatch] = useReducer(PersonaReducer,initialState);


    const getNextPersona = async () => {
        dispatch({
            type:GET_PERSONA_REQUEST
        }); 

        await GetActiva().then((response) => {

            if(state.persona != null) {
                dispatch({
                    type:SET_VISTO_PERSONA_ACTUAL
                });
            }
 
            dispatch({
                type:GET_PERSONA_SUCCESS,
                payload: response
            });
            

        },
        (error) => {

            showError(error,'error');

        })
    }

    /*** ERRORES ****/
    const showError = (error,category) => {
        if(error.length > 0)
            Swal.fire('Error',error,category);
    }

    return (
        <PersonasContext.Provider value={{
             persona:state.persona, 
             ultimos:state.ultimos,
             loadingPersona:state.loadingPersona,
             vistos:state.vistos,
             getNextPersona
        }}>
            {props.children}
        </PersonasContext.Provider>
    );
};

export default PersonaState;