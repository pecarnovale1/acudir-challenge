﻿using DataAccess;
using DataAccess.Interfaces;
using Entities.Domain;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acudir.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PersonaController : ControllerBase
    {
        private readonly IPersonaService  _personaService;

        public PersonaController(IPersonaService personaService)
        {
            _personaService = personaService;
        }

        [HttpGet]
        public async Task<ActionResult<Persona>> GetActivo()
        {
            var persona = await _personaService.GetActivo();
         
            return Ok(persona);
        }

        [HttpDelete]
        public async Task<ActionResult<int>> Delete(int id)
        {
            int idDeleted = await _personaService.Delete(id);

            return Ok(idDeleted);
        }
    }
}
