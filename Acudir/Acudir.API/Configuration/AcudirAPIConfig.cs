﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acudir.API.Configuration
{
    public class AcudirAPIConfig
    {
        public string AcudirConnection { get; set; }
    }
}
