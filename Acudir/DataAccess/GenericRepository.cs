﻿using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;
        public GenericRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<T>> GetAsync()
        {
            return await _unitOfWork.Context.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> whereCondition = null,
                                  Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = _unitOfWork.Context.Set<T>();

            if (whereCondition != null)
            {
                query = query.Where(whereCondition);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        public async Task<int> GetCount(Expression<Func<T, bool>> whereCondition = null)
        {
            IQueryable<T> query = _unitOfWork.Context.Set<T>();

            if (whereCondition != null)
            {
                query = query.Where(whereCondition);
            }

            return await query.CountAsync();
        }




        public async Task<T> CreateAsync(T entity)
        {
            try
            {
                var save = await _unitOfWork.Context.Set<T>().AddAsync(entity);
            }
            catch (Exception)
            {
                throw;
            }

            return entity;
        }

        public async Task<T> DeleteAsync(T entity)
        {
            _unitOfWork.Context.Entry(entity).State = EntityState.Deleted;

            return entity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            _unitOfWork.Context.Entry(entity).State = EntityState.Modified;

            return entity;
        }
    }
}
