﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Domain
{
    public class Persona
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Provincia { get; set; }
        public int DNI { get; set; }
        public int Telefono { get; set; }
        public bool Activo { get; set; }
        public string Email { get; set; }
    }
}
