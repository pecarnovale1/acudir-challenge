﻿using Entities.Domain;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IPersonaService
    {
        Task<Persona> GetActivo();
        Task<int> Delete(int id);
    }
}
