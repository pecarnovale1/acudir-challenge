﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Entities.Domain;
using DataAccess.Interfaces;
using System.Linq;
using DataAccess;

namespace Services
{
    public class PersonaService : IPersonaService
    {
        private readonly IGenericRepository<Persona> _personaRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PersonaService(IGenericRepository<Persona> personaRepository, IUnitOfWork unitOfWork)
        {
            _personaRepository = personaRepository;
            _unitOfWork = unitOfWork;
        }


        public async Task<Persona> GetActivo()
        {
            IEnumerable<Persona> activos = await _personaRepository.GetAsync(x => x.Activo == true);

            List<int> activosIds = activos.Select(x => x.Id).Distinct().ToList();

            Random r = new Random();
            int rInt = activosIds.OrderBy(x => r.Next()).Take(1).First();

            IEnumerable<Persona> personas = await _personaRepository.GetAsync(x => x.Id == rInt && x.Activo == true);
            return personas.FirstOrDefault();
        }

        public async Task<int> Delete(int id)
        {
            IEnumerable<Persona> personas = await _personaRepository.GetAsync(x => x.Id == id && x.Activo == true);
            
            Persona persona = personas.FirstOrDefault();

            persona.Activo = false;

            if (await _personaRepository.UpdateAsync(persona) != null)
                _unitOfWork.Commit();

            return persona.Id;

        }
    }
}
